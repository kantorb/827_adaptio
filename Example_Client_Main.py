# Kántor Balázs, Kristófi János Mihály 2022

import time
from Client import SocketClient
import json
import numpy as np
import math
import random
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers
import tensorflow.keras.initializers as initializers
import matplotlib.pyplot as plt
from collections import deque



# Tanító mód: True vagy betöltés: False
Training_on = False

STATE_VAR_NUM=84  #81 érték a látómezőből, x koord, y koord, méret,   SUM=84 state változó tick,

learning_rate = 0.01
num_episodes = 500 # epizódok száma a tanításra
train_episodes = 500 # tanítási epizódok száma
experience_replay = deque(maxlen = 25_000) # kimentett helyzetek memória mérete

#q háló létrehozása, 
def construct_q_network(state_dim: int, action_dim: int) -> keras.Model:
    
    learning_rate = 0.01
    init = tf.keras.initializers.HeUniform()
    q_network = keras.Sequential()
    q_network.add(keras.layers.Dense(100, input_shape=(state_dim,), activation='elu', kernel_initializer=init))
    q_network.add(keras.layers.Dense(20, activation='elu', kernel_initializer=init))
    q_network.add(keras.layers.Dense(action_dim, activation='linear', kernel_initializer=init))
    q_network.compile(loss=tf.keras.losses.Huber(), optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate), metrics=['accuracy'])

    return q_network

def train_model(experience_replay, target_model, model):
    learning_rate = 0.07 #learing rate
    discount_factor = 0.62 

    Minimal_replay_size= 200
    #Amíg nem gyűlt össze elegendő számú minta térjen vissza
    if Minimal_replay_size>len(experience_replay): 
        return
    
    batch_size=100
    mini_batch = random.sample(experience_replay, batch_size)
    current_states = np.array([transition[0] for transition in mini_batch])
    current_qs_list = model.predict(current_states)
    new_current_states = np.array([transition[3] for transition in mini_batch])
    future_qs_list = target_model.predict(new_current_states)
    
    X = []
    Y = []
    for index, (observation, action, reward, new_observation) in enumerate(mini_batch):
        max_future_q = reward + discount_factor * np.max(future_qs_list[index])

        current_qs = current_qs_list[index]
        current_qs[action] = (1 - learning_rate) * current_qs[action] + learning_rate * max_future_q

        X.append(observation)
        Y.append(current_qs)

    history=model.fit(np.array(X), np.array(Y), batch_size=batch_size, verbose=0, shuffle=True)
    data2save=np.array((history.history['accuracy'], history.history['loss']))
    with open("accuracy_loss.csv", "ab") as f:
        np.savetxt(f, data2save, delimiter=",")


#Remote DQN bot osztály
class MyRemoteAIStrategy:

    def __init__(self, path=None):
         # Dinamikus viselkedéshez szükséges változók definíciója
        self.state=np.zeros((STATE_VAR_NUM)) #állapot inicializálása
        
        self.oldcounter = 0
        self.action_num=0   # az ágnes lépése számként
        self.size=0 # ágens score-ja, mérete
        self.oldsize = 0 # egyel korábbi méret
        self.oldpos = 0 # korábbi pozíció
        self.oldposJson = None
        
        self.episode = 2 # aktuálisan futó játék sorszáma
        self.reward = 0 # lépésért kapott jutalom
        self.episode_reward = 0 # adott játékban kapott összes jutalom
        self.sum_reward = 0 # futtatás alatti szerzett összes jutalom
        self.sum_score = 0 # futtatás alatti szerzett összes pontszám

        self.total_rewards=np.zeros(train_episodes) # játékonként a jutalom és pontszám
        self.total_score=np.zeros(train_episodes)
        self.total_avg_reward=np.zeros(train_episodes)
        self.total_avg_score=np.zeros(train_episodes)
        self.steps_update_target_model = 1 # target háló frissítésére számláló változó

        self.epsilon = 0  # az epsilon greedy algoritmushoz
    

    def get_reward_and_state(self,fulljson):
        #valtozó inicializálás
        new_state=np.zeros(STATE_VAR_NUM)
        reward_food=0
        reward_moved=0
        reward_alive=0
        reward_hunt=0
        reward_wall=0
        

        jsonData = fulljson["payload"] 
        if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
            #mozgás reward
            if self.oldposJson is not None:
                if tuple(self.oldposJson) == tuple(jsonData["pos"]):
                     self.oldcounter += 1
                     reward_moved=-self.oldcounter
                     #minnél tovább helyben marad annál nagyobb büntetés
                else:
                     reward_moved=2
                     self.oldcounter = 0 # ha elmozdultakkor reward
            
            # reward ha életben van
            if jsonData["active"]:
                reward_alive = 1
                self.size = jsonData["size"] # méret frissítés
                self.oldposJson = jsonData["pos"].copy() # pozíció frissítés
                
            else: reward_alive = 0

            # méretnövekedéssel arányos reward
            if self.oldsize < self.size:
                reward_size = (self.size - self.oldsize)*6
                self.oldsize = self.size
            else:
                reward_size = 0
            

            #State frissítése az adatok alapján
            vals = []
            for field in jsonData["vision"]:
                if field["player"] is not None:
                    if tuple(field["relative_coord"]) == (0, 0): #az ágens mezője, tehát önmaga
                        if 0 < field["value"] <= 3:
                            vals.append(field["value"])
                            reward_food+=field["value"]*2 # Ha kajára lép akkor nagy reward
                        elif field["value"] == 9:
                            vals.append(-1)
                            reward_wall+=-1/5
                        else:
                            vals.append(0)
                    elif field["player"]["size"] * 1.1 < jsonData["size"]: # más játékos fieldje
                        vals.append(3) # hogy nagy kajának lássa
                        dist = np.linalg.norm(np.array((tuple(field["relative_coord"])))-np.array((0,0))) #minnél közelebb van a másik játékoshoz annál nagyobb jutalom
                        #print('Ellenfél a látómezőn \n  rel koord', tuple(field["relative_coord"]), 'dist:',dist, 'méret:',field["player"]["size"])
                        reward_hunt=6-dist # jutalom ha a távolsag kicsi az ellenféltől
                    else:

                        vals.append(-1)  # ha nagyobb nála akkor falnak nézi, ki kell kerülni
                else:
                    if 0 < field["value"] <= 3:
                        vals.append(field["value"])
                        dist = np.linalg.norm(np.array((tuple(field["relative_coord"])))-np.array((0,0))) #minnél közelebb van az ételhez annál nagyobb jutalom
                        #print('Kaja a latómezőben \n rel koord', tuple(field["relative_coord"]), 'dist:',dist, 'kajaméret:',field["value"])
                        reward_food+=(15-field["value"]*dist)/2 #közel és nagy pontú kaját pontozza
                    elif field["value"] == 9:
                        vals.append(-1)
                        #dist = np.linalg.norm(np.array((tuple(field["relative_coord"])))-np.array((0,0))) #minnél távolabb van a fal játékoshoz annál nagyobb jutalom
                        reward_wall+=-1/5                        
                    else:
                        vals.append(0)
        #jutalom a jutalmak összege
        reward=reward_alive+reward_food+reward_moved+reward_size+reward_hunt+reward_wall
        
        if(reward_alive==0) or reward < 0: # ha meghal vagy negatívba megy át akkor nincs jutalom
            reward=0   
        
        vals.append(jsonData["pos"][0]/40) # ágens x pozíciója
        vals.append(jsonData["pos"][1]/40) # ágens y pozíciója 
        vals.append(jsonData["size"]) # játékos mérete
        values = np.array(vals) #84 elem a látómezőben
        
        new_state=values # visszatérendő új állapot

        return reward, new_state

    #random mozgás generáló függvény
    def getRandomAction(self):
        action_num=np.random.randint(0, 8)
        return action_num
    # akció számból jsonba küldendő stringet készítő függvény
    def getActionNum2ActionString(self,action_num):
        if action_num == 0:
            actstring = "00"
        elif action_num == 1:
            actstring = "0+"
        elif action_num == 2:
            actstring = "0-"
        elif action_num == 3:
            actstring = "+0"
        elif action_num == 4:
            actstring = "-0"
        elif action_num == 5:
            actstring = "++"
        elif action_num == 6:
            actstring = "--"
        elif action_num == 7:
            actstring = "+-"
        elif action_num == 8:
            actstring = "-+"
        else:
            actstring = "00"
        return actstring

    def processObservation(self, fulljson, sendData):
        # Játék indítása masterként, ha indításra kész
        
         if fulljson["type"] == "readyToStart" and self.episode < train_episodes:
             print("Game is ready, starting in 1")
             time.sleep(0.2)
             
             self.state = np.zeros((STATE_VAR_NUM)) # újrainicializálás minden játék elején
             self.episode_reward = 0
             self.episode += 1

             sendData(json.dumps({"command": "GameControl", "name": "master",
                                  "payload": {"type": "start", "data": None}}))
             
        
         if fulljson["type"] == "started":
             print("Startup message from server.")
             print("Ticks interval is:",fulljson["payload"]["tickLength"])
        
        # Játék adatok feldolgozása
         if fulljson["type"]=="gameData":
            #ha él
            if fulljson["payload"]["active"]:
                actstring = ''
                
                if Training_on == False: # ha nincs tanítás, akkor is fixen 5% esélyel random mozgás
                    self.epsilon=0.05
                #eplsilon greedy algoritmus     
                epsilon = np.random.rand()
                if epsilon <= self.epsilon:
                # random action ha az epszilon értéknél kissebbet dobunk
                    action = self.getRandomAction()
                    actstring=self.getActionNum2ActionString(action)
                else:
                # legmagasabb értékú actiona háló kimenetéből ha nagyobbat dobunk
                    environment = self.state # a környezet állapota
                    environment_reshaped = environment.reshape([1, environment.shape[0]]) 
                    predicted = model.predict(environment_reshaped).flatten() # neurális háló lépése
                    self.action_num= np.argmax(predicted) # akció meghatározása, a legnagyobb értékű indexét kiszedjük
                    actstring=self.getActionNum2ActionString(self.action_num) # string actionné változtatjuk
                    print('AI_move: ', actstring)
                # akció kiküldése az enginenek
                sendData(json.dumps({"command": "SetAction", "name": "Blue", "payload": actstring})) # választott lépés kiküldése
                
                # kapott jutalom és a lépés utáni környezet frissítése
                self.reward, new_state = self.get_reward_and_state(fulljson) 
                
                # ha a tanuló módba van
                if Training_on == True: 
                     self.margin = np.sum(self.total_rewards)/(self.episode*300)*0.5; # határérték kiszámolása
                     
                     if len(experience_replay) < 5000:     #az első 5000 lépés eredményét minden esetben letárljuk a memóriába
                         experience_replay.append([self.state, self.action_num, self.reward, new_state])
                     if len(experience_replay) >= 5000  and self.reward >= self.margin:       # az 5000 lépésen túl már csak a határértéken felül teljesítő lépéseket
                         experience_replay.append([self.state, self.action_num, self.reward, new_state])
                    # 10 lépésenként tanítjuk a hálót, a memóriából eseteket visszajátszva                                               
                     if self.steps_update_target_model % 10 == 0: 
                        train_model(experience_replay, target_model, model)

                    # 100 lépés után a target háló súlyait is frissítjük, átmásoljuk a tanított alap hálóból
                     if self.steps_update_target_model % 100 == 0: 
                        target_model.set_weights(model.get_weights())
                        self.steps_update_target_model = 0
                        
                     self.state = new_state # állapotok frissítése
                     self.steps_update_target_model += 1 # tanítási lépésszám növelése
                     self.episode_reward += self.reward # szumma reward aktualizálása
         
          # Ha a játék véget ért
         if fulljson["type"] == "leaderBoard":
             print("Game finished after",fulljson["payload"]["ticks"],"ticks!")
             print("Leaderboard:")
   
            # szerzett pontszám kiolvasása az adatcsomagból
             for score in fulljson["payload"]["players"]:
                 print(score["name"],score["active"], score["maxSize"])
                 if score["name"] == 'Blue':
                     self.total_score[self.episode-1]=score['maxSize']
                     self.sum_score += score['maxSize']
   
               
            # játék alaphelyzetbe állítása
             if Training_on==True:
                 sendData(json.dumps({"command": "GameControl", "name": "master",
                                  "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))   
             
                # modellek mentése minden episode végén
                 model.save('./models2/model_FINAL2')
                 target_model.save('./models2/target_model_FINAL2')
             
             time.sleep(.5)
            # epsilon greedy frissítése
             self.epsilon = 0.05 + (1 - 0.05) * np.exp(-0.01 * (self.episode-1)) 
            # kapott jutalmak kimentése
             self.total_rewards[self.episode-1]=self.episode_reward 
             self.sum_reward += self.episode_reward
             
            # utolsó 10 játék összes rewardjából és elért pontszámából mozgó átlag számítása 
             if (self.episode-1) > 10:
                  self.sum_reward -= self.total_rewards[self.episode-11]
                  avg_reward = self.sum_reward/10
                  self.sum_score -= self.total_score[self.episode-11]
                  avg_score = self.sum_score/10
             else:
                  avg_reward = self.sum_reward/(self.episode-1)
                  avg_score = self.sum_score/(self.episode-1)
            
             self.total_avg_reward[self.episode-1]=avg_reward 
             self.total_avg_score[self.episode-1]=avg_score   

            # utolsó játék eredményeinek kiiratása
             print("\n")
             print("Episode: ", self.episode-1)
             print("Episode_reward: ", self.episode_reward)
             print("Episode_score: ", self.total_score[self.episode-1])
             print("AVG_reward: ", avg_reward)
             print("AVG_score: ", avg_score)
             print("len(experience_replay): ", len(experience_replay))
             print("\n==========================================")
             print("\n")
             
             #rewardok és scorok epizódonkkénti kimentése fileba
             data2save=np.array((self.total_rewards,self.total_avg_reward, self.total_score,  self.total_avg_score))
             np.savetxt("foo_talanjoleszPLS.csv", data2save, delimiter=",")
             
             # néhány változó alaphelyzetbe állítása a játék végén
             self.episode_reward = 0
             self.size = 0
             self.oldsize = 5
       

    
#model = construct_q_network(STATE_VAR_NUM,9)
model = keras.models.load_model('./models2/target_model_FINAL2')
if __name__=="__main__":
    # Példányosított stratégia objektum
    hunter = MyRemoteAIStrategy()

    # neurális háló létrehozása vagy betöltése korábbi mentésből
    if Training_on == True:
        model = construct_q_network(STATE_VAR_NUM,9)
    else:
        pass
        #model = keras.models.load_model('./models2/model_FINAL')

    
    # target háló felépítése, és a súlyok bemásolása az alap hálóból
    target_model = construct_q_network(STATE_VAR_NUM,9)
    target_model.set_weights(model.get_weights())

    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)
    client = SocketClient("10.0.0.113", 42069, hunter.processObservation)

    # Kliens indítása
    client.start()
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "name": "Blue", "payload": None}))

    #plotresults(hunter. total_rewards,hunter.total_avg_reward, hunter.total_score,  hunter.total_avg_score)

    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.