# AdaptIO
Author: Kántor Balázs, Kristófi János Mihály 2022

## Requirements:
- python 3
- numpy
- json

- tensorflow v2
- models2/target_model_FINAL2

Change "Training_on" variable in Example_Client_Main.py to FALSE for loaded network, or TRUE to train the network.
